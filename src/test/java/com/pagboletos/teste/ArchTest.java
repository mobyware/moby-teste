package com.pagboletos.teste;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.pagboletos.teste");

        noClasses()
            .that()
                .resideInAnyPackage("com.pagboletos.teste.service..")
            .or()
                .resideInAnyPackage("com.pagboletos.teste.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.pagboletos.teste.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
