/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pagboletos.teste.web.rest.vm;
